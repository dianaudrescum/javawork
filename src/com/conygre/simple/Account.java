package com.conygre.simple;

public class Account {
    private double balance;
    private String name;
    private static double interestRate = 2.0;

    public Account(String myS, double myD) {
       name = myS;
       balance = myD;


    }

    public Account(){// constructor calling another constructor in the same class

        this("Diana", 70000);
    }

    public static void setInterest(double interest){
        interestRate = interest;

    }

    public static double getInterest(){
        return interestRate;
    }


    public String getName(){
        return name;
        //System.out.println(name);

    }

    public void setName(String newName){
        this.name = newName;
    }

    public double getBalance(){
        return balance;
    }

    public void setBalance(double newBalance){
        this.balance = newBalance;
    }

    public void addInterest(){
        balance = balance * interestRate;
    }

    public boolean withdraw (double amount){
        boolean flag = false;
        if ((balance - amount) > 0){
            balance = balance - amount;
            flag = true;

        }
        else System.out.println("Not enough money");
        return flag;
    }

    public boolean withdraw(){
        return withdraw(20);
    }



}

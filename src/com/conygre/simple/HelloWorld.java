package com.conygre.simple;

public class HelloWorld {

    public static void main(String[] args) {

        int normal_int = 2; //var & function names start with lowercase
        short firstShort = 3;
        short secondFirst = (short) (firstShort + 2); // casting to short
        System.out.println("Hello");

        String make, model;
        double engineSize;
        byte gear;

        make = "Ford";
        model = "Fiesta";
        engineSize = 3.0;
        gear = 5;
        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);

        /*The make is x
        The gear is y
        The engine size is z*/

        short speed;
        speed = (short) (gear * 20);
        System.out.println("Speed is " + speed);

        /*Firstly, put in some logic to print out either that the car is a powerful car or a weak car
        based on the engine size, for example, if the size is less than or equal to 1.3.*/
        if (engineSize <= 1.3) {
            System.out.println("Car is weak");
        } else {
            System.out.println("Car is powerful");
        }

        if (gear == 5) {
            System.out.println("Speed should be 45mph");
        }

        /*
        loops around all the years between 1900 and the year 2000
        and print out all the leap years
        * */
        boolean leap = false;
        for (int i = 1900; i <= 2000; i++) {
            if (i % 4 == 0) {

                // if the year is century
                if (i % 100 == 0) {

                    // if year is divided by 400
                    // then it is a leap year
                    if (i % 400 == 0)
                        leap = true;
                    else
                        leap = false;
                }

                // if the year is not century
                else
                    leap = true;
            }
            else
                leap = false;

            if (leap)
                System.out.println(i + "\n");

        }
    }
}











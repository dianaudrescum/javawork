package com.conygre.simple;

import com.conygre.simple.Account;

public class TestClass {
    public static void main (String[] args){

        Account myAccount = new Account();
        myAccount.setName("Diana");
        myAccount.setBalance(10000);

        System.out.println(myAccount.getName());
        System.out.println(myAccount.getBalance());

        myAccount.addInterest();
        System.out.println(myAccount.getBalance());

        Account [] arrayOfAccounts = new Account [5]; //instantiating static size array
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

        for(int i = 0; i < arrayOfAccounts.length; i++){
            //instantiate com.conygre.simple.Account object first
            arrayOfAccounts[i] = new Account();
            //populate array
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].setBalance(amounts[i]);
            System.out.println(arrayOfAccounts[i].getName() + " " +arrayOfAccounts[i].getBalance());

            arrayOfAccounts[i].addInterest();
            System.out.println("New Balance is: " + arrayOfAccounts[i].getBalance());
            //arrayOfAccounts[i].withdraw(3000);
            arrayOfAccounts[i].withdraw();
            System.out.println("Balance after withdraw " + arrayOfAccounts[i].getBalance());



                }
            }

        }

        /*for (Plane plane : arrayOne) {
    System.out.println(plane.getDestination());
}*/









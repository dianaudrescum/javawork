public interface Feedable {

    void feed();
    //for default implementation

    /*default void feed() {
        System.out.println("Feed generic");
    }*/
}
